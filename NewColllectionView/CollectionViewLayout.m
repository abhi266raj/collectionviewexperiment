//
//  CollectionViewLayout.m
//  NewColllectionView
//
//  Created by Abhiraj Kumar on 4/11/17.
//  Copyright © 2017 Experiment. All rights reserved.
//

#import "CollectionViewLayout.h"
#import <QuartzCore/CALayer.h>
#import <Foundation/NSObject.h>

@interface CollectionViewLayout() {
    NSMutableArray * array;
    NSMutableArray *allobject;
    UICollectionViewLayoutAttributes *att;
    NSIndexPath *indexpath2;
    NSMutableArray *allindexPath;
}

@end

@implementation CollectionViewLayout


- (CGSize)collectionViewContentSize{
    return CGSizeMake(400, 3000);
}

- (nullable NSArray<__kindof UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect{
    //return nil;
    CGFloat height = self.collectionView.contentOffset.y;
    CGFloat height2  = MAX(100, 400-height);
    NSLog(@"Abcd");
   ((UICollectionViewLayoutAttributes*) array[0][0]).frame = CGRectMake(0, height, self.collectionView.frame.size.width, height2);
    return allobject;
    UICollectionViewLayoutAttributes *layoutAttribute = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:[NSIndexPath indexPathWithIndex:0]];
    return @[layoutAttribute];
}// return an array layout attributes instances for all the views in the given rect


- (nullable UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewLayoutAttributes *attr =  array[indexPath.section][indexPath.row];
    
    return attr;
}

//- (nullable UICollectionViewLayoutAttributes *)layoutAttributesForSupplementaryViewOfKind:(NSString *)elementKind atIndexPath:(NSIndexPath *)indexPath{
//    return allobject[0];
//}

- (BOOL)shouldInvalidateLayoutForPreferredLayoutAttributes:(UICollectionViewLayoutAttributes *)preferredAttributes withOriginalAttributes:(UICollectionViewLayoutAttributes *)originalAttributes NS_AVAILABLE_IOS(8_0){
    return true;
}


- (nullable UICollectionViewLayoutAttributes *)layoutAttributesForDecorationViewOfKind:(NSString*)elementKind atIndexPath:(NSIndexPath *)indexPath{
    return nil;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds{
    
    //[self.collectionView.panGestureRecognizer locationInView:self.collectionView];
    return true;
}// return YES to cause the collection view to requery the layout for geometry information
- (UICollectionViewLayoutInvalidationContext *)invalidationContextForBoundsChange:(CGRect)newBounds{
    
    UICollectionViewLayoutInvalidationContext *context = [super invalidationContextForBoundsChange:newBounds];   // context.in
    [context invalidateItemsAtIndexPaths:allindexPath];
    //context.invalidatedItemIndexPaths = @[[NSIndexPath indexPathForRow:0 inSection:0]];
    return context;
}


-(void)prepareLayout{
    allindexPath = [[NSMutableArray alloc]init];
    indexpath2 = [NSIndexPath indexPathForRow:2 inSection:2];
    att = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexpath2];
    att.frame = CGRectMake(300,0,10,10);
    array = [[NSMutableArray alloc]init];
    allobject = [[NSMutableArray alloc]init];
    int section = [self.collectionView numberOfSections];
    for (int i = 0; i< section; i++){
        NSMutableArray *temparray = [[NSMutableArray alloc]init];
        int row = [self.collectionView numberOfItemsInSection:i];
        for (int j = 0; j<row;j++){
             UICollectionViewLayoutAttributes *layoutAttribute = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:[NSIndexPath indexPathForRow:j inSection:i]];
            layoutAttribute.frame = CGRectMake(j*150,150*i,100,130);
            if (j == 0 && i == 0){
                layoutAttribute.zIndex = 100;
                 layoutAttribute.frame = CGRectMake(50,00,400,430);
            }else{
                layoutAttribute.zIndex = 99;
            }
            [allindexPath addObject:[NSIndexPath indexPathForRow:j inSection:i]];
            [temparray addObject:layoutAttribute];
            [allobject addObject:layoutAttribute];
        }
        [array addObject:temparray];
}
}



- (UICollectionViewLayoutAttributes *)layoutAttributesForInteractivelyMovingItemAtIndexPath:(NSIndexPath *)indexPath withTargetPosition:(CGPoint)position NS_AVAILABLE_IOS(9_0){
    return allobject[0];
}


- (nullable UICollectionViewLayoutAttributes *)initialLayoutAttributesForAppearingItemAtIndexPath:(NSIndexPath *)itemIndexPath{
    
    UICollectionViewLayoutAttributes *attr = [super initialLayoutAttributesForAppearingItemAtIndexPath:itemIndexPath];
     attr.frame = CGRectMake(attr.frame.origin.x, attr.frame.origin.y, 200, 200);
  //  attr.transform = CGAffineTransformMakeScale(2, 2);
    
   // attr.transform = CGAffineTransformRotate(CGAffineTransformMakeScale(0.7, 0.7),100*M_PI);
    //
    //attr.center = CGPointMake(CGRectGetMidX(attr.bounds), CGRectGetMaxY(attr.bounds));
    
    return attr;
    
//    UICollectionViewLayoutAttributes *attributes = [super initialLayoutAttributesForAppearingItemAtIndexPath:itemIndexPath];
//    //attributes.alpha = 1.0;
//    //attributes.hidden = true;
//    //if (itemIndexPath.row == 1 || itemIndexPath.section == 1 ){
//        attributes.frame = CGRectMake(attributes.frame.origin.x-100,attributes.frame.origin.y,200,200);
//    //}
//    att.transform = CGAffineTransformMakeScale(0, 0);
//
//    return attributes;
//    //return nil;
////    if (itemIndexPath.row == indexpath2.row && itemIndexPath.section== indexpath2.section){
////        //return att;
////        return array[itemIndexPath.section][itemIndexPath.row];
////    }
//    return array[itemIndexPath.section][itemIndexPath.row];
    
    
}



- (nullable UICollectionViewLayoutAttributes *)finalLayoutAttributesForDisappearingItemAtIndexPath:(NSIndexPath *)itemIndexPath{
    UICollectionViewLayoutAttributes *attr = [super finalLayoutAttributesForDisappearingItemAtIndexPath:itemIndexPath];
    
    
    //attr.transform = CGAffineTransformMakeScale(0.5, 0.5);
    //CGAffineTransformScale(<#CGAffineTransform t#>, 0,5, 0.5)
//    attr.frame = CGRectMake(0, 0, 10, 10);
    
   //attr.center = CGPointMake(CGRectGetMidX(attr.bounds), CGRectGetMaxY(attr.bounds));
    attr.frame = CGRectMake(attr.frame.origin.x, attr.frame.origin.y, 200, 200);
    return attr;
    UICollectionViewLayoutAttributes *attributes = [super finalLayoutAttributesForDisappearingItemAtIndexPath:itemIndexPath];
    //attributes.alpha = 0.0;
   // if (itemIndexPath.row == 1 || itemIndexPath.section == 1 ){
    attributes.frame = CGRectMake(attributes.frame.origin.x-100,attributes.frame.origin.y,40,40);
    //}
    att.transform = CGAffineTransformMakeScale(0, 0);
    
//    CABasicAnimation * transformAnimation = [CABasicAnimation alloc]initWit
//    transformAnimation.fromValue = NSValue(CATransform3D:startTransform)
//    transformAnimation.toValue = NSValue(CATransform3D:endTransform)
//    transformAnimation.duration = 0.4
//    transformAnimation.beginTime = CACurrentMediaTime() + Double((0.04 * Double(itemIndexPath.row)))
//    transformAnimation.fillMode = kCAFillModeForwards
    return attributes;
//   if (itemIndexPath.row == indexpath2.row && itemIndexPath.section== indexpath2.section){
//        return att;
//        //return array[itemIndexPath.section][itemIndexPath.row];
//    }
//    return array[itemIndexPath.section][itemIndexPath.row];

    
    
}



//-(CABasicAnimation *)animation2{
//    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
//    transformAnimation.duration = 1.0f;
//    CGFloat height = [self collectionViewContentSize].height;
//    
//    transformAnimation.fromValue = [NSValue valueWithCATransform3D:CATransform3DMakeTranslation(0, 2*height, height)];
//    transformAnimation.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeTranslation(0, attributes.bounds.origin.y, 0)];
//    transformAnimation.removedOnCompletion = NO;
//    transformAnimation.fillMode = kCAFillModeForwards;
//    return transformAnimation;
//    
//    //attributes.transformAnimation = transformAnimation;
//    //return attributes;
//}




@end
